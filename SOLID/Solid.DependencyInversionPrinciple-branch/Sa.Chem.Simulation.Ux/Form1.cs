﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace Sa.Chem.Simulation.Ux
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			AllocConsole();

			using (Pyps.Controller p = new Pyps.Controller())
			{
				Console.WriteLine(p.RunOneFeed());
			}

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}

		[DllImport("kernel32.dll", SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool AllocConsole();

		private void button2_Click(object sender, EventArgs e)
		{
			AllocConsole();

			using (Pyps.Controller p = new Pyps.Controller())
			{
				Console.WriteLine(p.RunFromQueue());
			}

			Console.WriteLine();
			Console.WriteLine("Press any key to continue.");
			Console.ReadKey();
		}
	}
}
